# The Art of Coarse Stokes

This repository contains the code associated with the manuscript:

"The art of coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets"

D. J. Smith and M. T. Gallagher


To initialise submodules use
git submodule init

To update submodules use
git submodule update --recursive --remoter
