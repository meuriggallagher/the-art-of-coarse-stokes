function x = DiscretiseTorus(R, r, dR, x0)
%DISCRETISETORUS Create discretised torus
%
% X = DISCRETISETORUS(R, r, DR, X0) creates a discretised torus with
% central-axis length R, tube-axis length r, approximate spacing DR,
% located at X0
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%% Number of points in theta
nTh = round(2 * pi * r / dR);
th = linspace(0, 2 * pi, nTh + 1)'; th = th(1 : end - 1);

% For each th discretise according to number of points in r
rTh = 2 * pi * (R + r * cos(th));
nPh = ceil(rTh / dR);

% Discretise in ph
x1 = NaN(sum(nPh), 1);
x2 = NaN(sum(nPh), 1);
x3 = NaN(sum(nPh), 1);

for iPh = 1 : numel(nPh)
    ph = linspace(0, 2 * pi, nPh(iPh) + 1)'; ph = ph(1 : end - 1);
    
    ind = sum(nPh(1 : iPh - 1)) + 1 : sum(nPh(1 : iPh));
    
    x1(ind) = x0(1) + (R + r * cos(th(iPh))) * cos(ph);
    x2(ind) = x0(2) + (R + r * cos(th(iPh))) * sin(ph);
    x3(ind) = x0(3) + repmat(r * sin(th(iPh)), nPh(iPh), 1);
end

x = [x1 ; x2; x3];

end
