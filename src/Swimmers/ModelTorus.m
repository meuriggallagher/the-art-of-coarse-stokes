function [x, v, X] = ModelTorus(~, model)
%MODELTORUX Extract position and velocity for model torus
%
% [X, V, XX] = MODELTORUS(t, model) extracts force discretisation X,
% quadrature discretisation XX and velocity of force points V at time t
% from torus model
%
% input:  
%         t          time (not used)
%         model      struct of torus model from NEAREST
%
% output: X          Force discretisation
%         V          Velocities of force nodes
%         XX         Quadrature discretisation
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%% Rotate points
% Rotation angle = rotational velocity * t
dt=0.001;
th = model.omega * [- dt/2, dt/2];

xx = NaN(numel(model.x), 3);
for ii = 1 : numel(th)
    xx(:, ii) = RotatePoints(model.x, [0; 0; 0], th(ii), 1);    
end

% Calculate velocities
v = (xx(:, 2) - xx(:, 1)) / dt;

x = model.x;
X = model.X;

end
