function [x, X, NN] = GenerateTorus(torusSize, torusDiscr, x0, discrType, procFlag, blockSize)
%GENERATETORUS Create torus discretisations
%
% [X, XX, NN] = GENERATETORUS(SIZE, DISCR, X0, DISCRTYPE, PROCFLAG, BLOCKSIZE) creates a discretised torus with
% force discretisation X, quadrature discretisation XX and
% nearest-neighbour matrix NN
%
% input:  
%         SIZE       [R, r] where R is central axis length and r i tube
%         axis length
%         DISCR      [dF, dQ] spacing for force and quadrature grids
%         X0         body frame origin
%         DISCRTYPE  'contained' or 'disjoint'
%         PROCFLAG   Processing flag - 'cpu' or 'gpu'
%         BLOCKSIZE  controls matrix assembly blocking size for numerical efficiency, 0.2 is a safe choice
%
% output: X          Force discretisation
%         XX         Quadrature discretisation
%         NN         NEAREST neighbour matrix 
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%% Set up
% Extract components
R = torusSize(1);
r = torusSize(2);

dR = torusDiscr(1);
dqR = torusDiscr(2);

%% Force nodes
x = DiscretiseTorus(R, r, dR, x0);

%% Quadrature nodes
if dR == dqR
    X = x;
    NN = speye(numel(x));
    
    return;
end

% Else create quadrature notes
X = DiscretiseTorus(R, r, dqR, x0);

%% Ensure disjoint or contained discretisaion
% Extract components
[x1, x2, x3] = ExtractComponents(x);
[X1, X2, X3] = ExtractComponents(X);

% Calculate minimum h for the quadrature discretisation
d = (X1 - X1').^2 + (X2 - X2').^2 + (X3 - X3').^2;
d = d + max(d(:)) * eye(size(d));
h = min(d(:));

% Remove points close to [x1, x2, x3]
d = (X1 - x1').^2 + (X2 - x2').^2 + (X3 - x3').^2;
iF = 1 : numel(x1);
iQ = 1 : numel(X1);
[~, IQ] = meshgrid(iF, iQ);
iQ = IQ(d < h / 10);
X1(iQ) = [];
X2(iQ) = [];
X3(iQ) = [];

% If contained discretisation is deisred then append x1, x2, x3
switch discrType
    case 'contained'
        X1 = [X1 ; x1];
        X2 = [X2 ; x2];
        X3 = [X3 ; x3];
end

X = [X1 ; X2 ; X3];

%% Create NN matrix and check that all force points have a corresponding quadrature point
NN = NearestNeighbourMatrix(X, x, procFlag, blockSize);

% Number of quadrature points per force point
nQuadPoints = sum(NN, 1);

% Check to see if there are any force points with no corresponding
% quadrature points
while min(nQuadPoints) < 1
    warning('%i force points exist with no nearest quadrature points, removing force points', numel(x(nQuadPoints < 1))/3)
    x(nQuadPoints < 1) = [];
    
    NN = NearestNeighbourMatrix(X, x, procFlag, blockSize);
    
    % Number of quadrature points per force point
    nQuadPoints = sum(NN, 1);
end

end