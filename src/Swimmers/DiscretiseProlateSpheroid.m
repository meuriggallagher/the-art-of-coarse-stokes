function x = DiscretiseProlateSpheroid(a, c, h)
%DISCRETISEPROLATESPHEROID Create discretised prolate spheroid
%
% X = DISCRETISEPROLATESPHEROID(A, C, H) creates a discretised spheroid with
% major-axis A, minor-axis C, approximate spacing H,
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%%
% In prolate spheroidal coordinates (mu, nu, phi)
alpha = sqrt(a^2 - c^2);
mu = acosh(a / alpha);

%% Spacing h in eta
L = pi * alpha * cosh(mu);
nL = ceil(L / h);
nu = linspace(0, pi, nL);

%%
x1 = [-a ; a];
x2 = [0 ; 0];
x3 = [0 ; 0];

for iL = 2 : nL - 1
    
    r = 2 * pi * alpha * sinh(mu) * sin(nu(iL));
    nR = ceil(r / h);
    phi = linspace(0, 2 * pi, nR + 1);
    phi = phi(1 : end - 1);
    
    % In cartesians
    xx = alpha * cosh(mu) * cos(nu(iL)) + 0 * phi;
    yy = alpha * sinh(mu) * sin(nu(iL)) .* cos(phi);
    zz = alpha * sinh(mu) * sin(nu(iL)) .* sin(phi);
    
    x1 = [x1 ; xx(:)];
    x2 = [x2 ; yy(:)];
    x3 = [x3 ; zz(:)];
end

x = [x1 ; x2 ; x3];

end
