function h = CalcMinH(x, X, blockSize)
%CALCMINH Calculate minimum distance between two sets of points
%
% H = CALCMINH(X, XX, BLOCKSIZE) calculates the minimum euclidean distance
% H between two sets of points X and XX, with BLOCKSIZE ensuring memory is
% not exceeded
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

M = numel(x) / 3;
Q = numel(X) / 3;

blockNodes=floor(blockSize(1)*2^27/(M / 3));

h = inf(M, 1);

[x1, x2, x3] = ExtractComponents(x);
[X1, X2, X3] = ExtractComponents(X);

for iMin = 1 : blockNodes : Q
        
    iMax = min(iMin + blockNodes - 1, Q);
    
    iRange= iMin : iMax;

    d = (x1 - X1(iRange)').^2 + (x2 - X2(iRange)').^2  + (x3 - X3(iRange)').^2;
    d(d == 0) = inf;
    
    h = min([h, d], [], 2);
end

h = sqrt(min(h));

end



