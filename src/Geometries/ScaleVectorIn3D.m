function xs = ScaleVectorIn3D(x, a)
%SCALEVECTORIN3D multiplies components of a 3D vector
%
% XS = SCALEVECTORIN3D(X,A) extracts the [x1; x2; x3] components of a
% vector X, and multiplies by scales A = [a1; a2; a3]
%
% Examples
% XS = SCALEVECTORIN3D(x, [1, 2, 3])
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

[x1, x2, x3] = ExtractComponents(x);
x1 = a(1) * x1;
x2 = a(2) * x2;
x3 = a(3) * x3;

xs = [x1; x2; x3];

end