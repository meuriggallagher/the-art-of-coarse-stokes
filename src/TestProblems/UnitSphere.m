function UnitSphere(params, epsilon, hh, procFlag, blockSize, dataDir)
%UNITSPHERE Calculate the GRM for a prolate unit sphere
%
% UNITSPHERE(PARAMS, EPSILON, H, PROCFLAG) calculates the GRM for a
% unit sphere for varied EPSILON and H using the Nystrom and Nystrom +
% Richardson methods
%
% Inputs:
%	PARAMS  		Sphere radius
%   EPSILON         Choices of regularisation parameter
%   H               Choices of discretisation lengthscale
%   PROCFLAG        'cpu' or 'gpu'
%   BLOCKSIZE       Memory useage threshold in GB
%   DATADIR         Output directory for saved filed
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%% Setup
% Origin of spheroid
x0 = [0, 0, 0];

% Major/minor axis lengths for spheroid
a = params;

% Nystrom choices for epsilon
epsilonN = epsilon;
nEpsilon = numel(epsilonN);

% Richardson choices for epsilon
epsilonRFactor = sqrt(2);
epsilonR = [epsilonRFactor.^2 .* epsilonN', epsilonRFactor.^1 .* epsilonN', ...
    epsilonRFactor.^0 .* epsilonN'];

% Domain = 'i' for stokeslests instead of blakelets
domain = 'i';

% Choices of refinement
nH = numel(hh);

% Exact solution for grand resistance tensor
A_an = 6 * pi * a * eye(3);
B_an = zeros(3);
C_an = 8 * pi * a^3 * eye(3);

R_an=[A_an, B_an'; B_an, C_an];

% Geometry
geomArea = pi * 4 * a^2;  

%% Richardson
R = cell(nH, nEpsilon, 3);
RError = NaN(nH, nEpsilon);
RErrorDum = NaN(nH, nEpsilon, 3);
h = NaN(nH, nEpsilon);
hMin = NaN(nH, 1);
walltime = NaN(nH, nEpsilon, 3);
sDOF = NaN(nH, nEpsilon);
CGpu = cell(nH, nEpsilon);
walltimeRich = NaN(nH, nEpsilon);
x = cell(nH, 1);

for iH = 1 : nH
    fprintf('h = %f\n\n', h(iH))

    fprintf('Iterating Nystrom simulations:\n\n')
    
    % Geometries   
    m = geomArea / hh(iH);
    m = round(m / 6);
    x{iH} = GenerateSphereDiscr(m, a);
    
    % Min spacing
    [xc, yc, zc] = ExtractComponents(x{iH});
    d = (xc-xc').^2 + (yc-yc').^2 + (zc-zc').^2;
    d = d + 1e6 * eye(size(d));
    hMin(iH) = min(d(:));
    
    for iE = 1 : nEpsilon
        fprintf('%i / %i\n', iE, nEpsilon)
        
        fprintf('\t')
        for iR = 1 : 3
            fprintf('%i ', iR)
            [R{iH, iE, iR}, walltime(iH, iE, iR), RErrorDum(iH, iE, iR), h(iH, iE), sDOF(iH, iE)] = ...
                GrandResistanceProlateSpheroid( ...
                x{iH}, x{iH}, speye(numel(x{iH})), x0, R_an, epsilonR(iE, iR), domain, blockSize, procFlag);
        end
        fprintf('\n')
    end
        
    fprintf('Combining Nystrom-Richardson simulations:\n\n')
    for iE = 1 : nEpsilon
        % Inverted matrix for quadratic extrapolation
        IM3 = RichardsonMatrixInverse(epsilonR(iE, :));
        
        fprintf('%i / %i\n', iE, nEpsilon)
        
        walltimeRich(iH, iE) = sum(walltime(iH, iE, :));
        
        tic
        
        CGpu{iH, iE} = IM3(1, 1) * R{iH, iE, 1} + IM3(1, 2) * R{iH, iE, 2} + IM3(1, 3) * R{iH, iE, 3};
        
        walltimeRich(iH, iE) = walltimeRich(iH, iE) + toc;
        
        RError(iH, iE) = norm(CGpu{iH, iE} - R_an) / norm(R_an);
    end
end

save(fullfile(dataDir, 'unit_sphere_ny_r.mat'), '-v7.3')

%% Extract Nystrom from richardson
R = R{:, :, 3}; %#ok<NASGU>
RError = squeeze(RErrorDum(:, :, 3)); %#ok<NASGU>
walltime = squeeze(walltime(:, :, 3)); %#ok<NASGU>

save(fullfile(dataDir, 'unit_sphere_ny.mat'), '-v7.3')

end
