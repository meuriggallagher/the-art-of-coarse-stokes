function RunAllTestProblems(procFlag, blockSize, dataDir)
%RUNALLTESTPROBLEMS runs the test problems for the below manuscript
%
% Output:
%   Data files saved to ../../data/
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%% Unit sphere
params = 1;
epsilon = [0.4, 0.2, 0.1, 0.05, 0.01];
h = 0.1 ./ 2.^[-4, -3.5, -3, -2.5, -2, -1.5, -1, -0.5, 0, 0.5];

UnitSphere(params, epsilon, h, procFlag, blockSize, dataDir)

clearvars params

%% Prolate sphere
params = {5, 1, [1; 0; 0]};
h = 0.1 ./ 2.^[-3, -2.5, -2, -1.5, -1, -0.5, 0];

ProlateSpheroid(params, epsilon, h, procFlag, blockSize, dataDir)

clearvars params

%% Sedimenting torus
params = {1 / 0.4, 1};
h = 0.1 ./ 2.^[-3, -2.5, -2, -1.5, -1, -0.5];

SedimentingTorus(params, epsilon, h, procFlag, blockSize, dataDir)

clearvars params

end
