function SedimentingTorus(params, epsilon, hh, procFlag, blockSize, dataDir)
%SEDIMENTINGTORUS Calculate the motion of a sedimenting torus
%
% SEDIMENTINGTORUS(PARAMS, EPSILON, H, PROCFLAG) calculates the motion of a
% torus sedimenting under gravity for varied EPSILON and H using the Nystrom, Nystrom +
% Richardson, and disjoint NEAREST methods
%
% Inputs:
%	PARAMS  		Torus parameters {R, r},
%                       R - central-axis length
%                       r - tube-axis length
%   EPSILON         Choices of regularisation parameter
%   H               Choices of discretisation lengthscale
%   PROCFLAG        'cpu' or 'gpu'
%   BLOCKSIZE       Memory useage threshold in GB
%   DATADIR         Output directory for saved filed
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%% Setup

% Origin of torus
x0{1} = [0, 0, 0];

% Size of torus
r = params{1};
R = params{2};
torusSize{1} = [R, r];

% Number of tori
nSwimmers = 1;

% Torus body frame
b1 = [1; 0; 0];
b2 = [0; 1; 0];
b3 = cross(b1, b2);
bodyFrame{1} = [b1, b2, b3];

% Discretisation type (contained/disjoint)
discrType{1} = 'disjoint';

% Nystrom choices for epsilon
epsilonN = epsilon;
nEpsilon = length(epsilonN);

% Richardson choices for epsilon
epsilonRFactor = sqrt(2);
epsilonR = [epsilonRFactor.^2 .* epsilonN', epsilonRFactor.^1 .* epsilonN', ...
    epsilonRFactor.^0 .* epsilonN'];

% NEAREST choice for epsilon
epsilonNe = 1e-6;

% Choices of refinement
nH = numel(hh);

% Quadrature factor for NEAREST
neFactor = 1 / 2;

% Solver parameters
tRange = [0, 4 * pi^2 * torusSize{1}(1) * torusSize{1}(2)];

% Domain = 'i' for stokeslests instead of blakelets
domain = 'i';

save(fullfile(dataDir, 'torus_params.mat'), '-v7.3')

clearvars -except dataDir

%% Ny
load(fullfile(dataDir, 'torus_params.mat')) %#ok<LOAD>

sol = cell(nH, nEpsilon);
swimmer = cell(nH, 1);
walltime = NaN(nH, nEpsilon);
h = NaN(nH, 1);
hMin = NaN(nH, 1);

probFlag = '';

for iH = 1 : nH
    fprintf('h = %f\n\n', hh(iH))
    
    % Initialise swimmer and boundary
    torusDiscr{1} = hh(iH) * ones(1, 2);
    
    [swimmer{iH}, NN]= SwimmersTorus( ...
        nSwimmers, ...
        @ModelTorus, ...
        torusSize, ...
        torusDiscr, ...
        discrType, ...
        x0, ...
        bodyFrame, ...
        procFlag, ...
        blockSize, ...
        0);
    
    boundary = [];
    NNMatrices = {NN, NN};
    
    fprintf('Iterating Nystrom simulations (%i dof):\n\n', numel(swimmer{iH}{1}.model.x))
    
    h(iH) = CalcDiscr_h(swimmer{iH}{1}.model.x, blockSize);
    
    [xc, yc, zc] = ExtractComponents(swimmer{iH}{1}.model.x);
    d = (xc - xc').^2 + (yc - yc').^2 + (zc - zc').^2;
    d = d + 1e6 * eye(size(d));
    hMin(iH) = min(d(:));
    
    for iE = 1 : nEpsilon
        fprintf('epsilon = %f (%i / %i)\n', epsilonN(iE), iE, nEpsilon)
        
        tic 
        
        sol{iH, iE} = SolveSedimentingProblem( ...
            tRange, ...
            swimmer{iH}, ...
            boundary, ...
            epsilonN(iE), ...
            domain, ...
            blockSize, ...
            'ode45', ...
            procFlag, ...
            probFlag, ...
            NNMatrices);
        
        walltime(iH, iE) = toc;
        
    end
    fprintf('\n')
    
end

save(fullfile(dataDir, 'torus_ny.mat'), '-v7.3')

clearvars -except dataDir

%% Ny-R
load(fullfile(dataDir, 'torus_params.mat')) %#ok<LOAD>

sol = cell(nH, nEpsilon);
swimmer = cell(nH, 1);
walltime = NaN(nH, nEpsilon);
h = NaN(nH, 1);
hMin = NaN(nH, 1);

probFlag = 'richardson';

for iH = 1 : nH
    fprintf('h = %f\n\n', hh(iH))
    
    % Initialise swimmer and boundary for NEAREST
    torusDiscr{1} = hh(iH) * ones(1, 4);
    
    [swimmer{iH}, NNSw1, NNSw2, NNSw3]= SwimmersTorusRichardson( ...
        nSwimmers, ...
        @ModelTorus, ...
        torusSize, ...
        torusDiscr, ...
        discrType, ...
        x0, ...
        bodyFrame, ...
        procFlag, ...
        blockSize, ...
        0);
    
    boundary = [];
    NNMatrices = {{NNSw1, NNSw2, NNSw3}, {NNSw1, NNSw2, NNSw3}};
    
    fprintf('Iterating NyR simulations (%i dof):\n\n', numel(swimmer{iH}{1}.model.x))
    
    h(iH) = CalcDiscr_h(swimmer{iH}{1}.model.x, blockSize);
    
    [xc, yc, zc] = ExtractComponents(swimmer{iH}{1}.model.x);
    d = (xc - xc').^2 + (yc - yc').^2 + (zc - zc').^2;
    d = d + 1e6 * eye(size(d));
    hMin(iH) = min(d(:));
    
    if iH < nH
        iE0 = 1;
    else
        iE0 = 2;
    end
    
    for iE = iE0 : nEpsilon
        fprintf('epsilon = %f (%i / %i)\n', epsilonN(iE), iE, nEpsilon)
        tic
        
        sol{iH, iE} = SolveSedimentingProblem( ...
            tRange, ...
            swimmer{iH}, ...
            boundary, ...
            epsilonR(iE, :), ...
            domain, ...
            blockSize, ...
            'ode45', ...
            procFlag, ...
            probFlag, ...
            NNMatrices);
        
        walltime(iH, iE) = toc;
        
    end
    fprintf('\n')
    
end

save(fullfile(dataDir, 'torus_ny_r.mat'), '-v7.3')

clearvars -except dataDir

%% Nearest for comparison
load(fullfile(dataDir, 'torus_params.mat')) %#ok<LOAD>

probFlag = '';

% Initialise swimmer and boundary for NEAREST
torusDiscr{1} = hh(end) * [1, neFactor^2];

[swimmer, NN]= SwimmersTorus( ...
    nSwimmers, ...
    @ModelTorus, ...
    torusSize, ...
    torusDiscr, ...
    discrType, ...
    x0, ...
    bodyFrame, ...
    procFlag, ...
    blockSize, ...
    0);

boundary = [];
NNMatrices = {NN, NN};

fprintf('Iterating NEAREST simulations (%i dof, %i quad):\n\n', ...
    numel(swimmer{1}.model.x), ...
    numel(swimmer{1}.model.X))

h = CalcDiscr_h(swimmer{1}.model.x, blockSize); %#ok<NASGU>

sol = SolveSedimentingProblem( ...
    tRange, ...
    swimmer, ...
    boundary, ...
    epsilonNe, ...
    domain, ...
    blockSize, ...
    'ode45', ...
    procFlag, ...
    probFlag, ...
    NNMatrices); %#ok<NASGU>

walltime = toc; %#ok<NASGU>

save(fullfile(dataDir, 'torus_ne.mat'), '-v7.3')

end
