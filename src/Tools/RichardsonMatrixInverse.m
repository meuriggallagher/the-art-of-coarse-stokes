function invM = RichardsonMatrixInverse(epsilon)
%RICHARDSONMATRIXINVERSE Inverse matrix for quadratic or cubic Richardson
%extrapolation
%
% INVM = RICHARDSONMATRIXINVERSE(EPSILON) produces the inverse matrix INVM
% for EPSILON containing either two or three choices of parameters
%
% Examples
% invM = RichardsonMatrixInverse([0.1, 0.2])
% invM = RichardsonMatrixInverse([0.1, 0.14, 0.2])
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

assert(length(epsilon) == 2 || length(epsilon) == 3, 'EPSILON must be length 2 or 3')

eps1 = epsilon(1);
eps2 = epsilon(2);

if length(epsilon) == 2
    invM = 1 / (eps1 - eps2) * [ - eps2, eps1 ; 1, -1];
    
elseif length(epsilon) == 3
    eps3 = epsilon(3);
    
    i1 = ((eps1 - eps2) * (eps1 - eps3));
    i2 = ((eps1 - eps2) * (eps2 - eps3));
    i3 = ((eps1 - eps3) * (eps2 - eps3));
    
    invM = [ ...
           eps2 * eps3  / i1,   - eps1 * eps3  / i2,   eps1 * eps2  / i3 ; ...
        - (eps2 + eps3) / i1,    (eps1 + eps3) / i2, -(eps1 + eps2) / i3 ; ...
                      1 / i1,              - 1 / i2,              1 / i3 ];
end

end
