function PlotFiguresFromManuscript(dataDir)
%PLOTFIGURESFROMMANUSCRIPT Plots all figures
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%% Figure 1 - unit sphere
try
   Figure1(dataDir);
catch
end

%% Figure 2 - prolate spheroid
try
   Figure2(dataDir);
catch
end

%% Figure 3 - torus
try
   Figure3(dataDir);
catch
end

%% Figure 4 - torus relative to NEAREST
try
   Figure4(dataDir);
catch
end

end
