function Figure2(dataDir)
%FIGURE2 Plot figure 2
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%%
figure

%% Colours
clr_gray_surface = [0.5, 0.5, 0.5];
clrs = parula(255);

subplot(3, 2, 1)
PlotGreySpheroid(5, 1, clr_gray_surface);

%% Force discr
x = DiscretiseProlateSpheroid(5, 1, 0.5);

subplot(3, 2, 1)
hold on
[x1, x2, x3] = ExtractComponents(x);
plot3(x1, x2, x3, '.')

%% DOF
load(fullfile(dataDir, 'prolate_spheroid_ny.mat'), 'hMin', 'sDOF')

subplot(3, 2, 2)
loglog(hMin, sDOF(:, 1))
xlabel('h')
ylabel('sDOF')

%% Nystrom
load(fullfile(dataDir, 'prolate_spheroid_ny.mat'), 'epsilonN', 'RError', 'nEpsilon')

subplot(3, 2, 3)
ConvergenceBoxPlot(fliplr(epsilonN), flipud(hMin), rot90(RError, 2), ...
    'fixedWidth', ...
    [1e-4, 1e0], ...
    clrs, ...
    'log')

xlabel('epsilon')
ylabel('h')
title('Ny relative error')

%% Nystrom vary h
subplot(3, 2, 5)
for iE = 1 : nEpsilon
    loglog(hMin, RError(:, iE), 'linewidth', 2)
    hold on
end

xlabel('h')
ylabel('relative error for fixed epsilon')
title('Ny')

%% Nystrom R
load(fullfile(dataDir, 'prolate_spheroid_ny_r.mat'), 'epsilonN', 'RError')

subplot(3, 2, 4)
ConvergenceBoxPlot(fliplr(epsilonN), flipud(hMin), rot90(RError, 2), ...
    'fixedWidth', ...
    [1e-4, 1e0], ...
    clrs, ...
    'log')

xlabel('epsilon')
ylabel('h')
title('NyR relative error')

%% Nystrom R vary h

subplot(3, 2, 6)
for iE = 1 : nEpsilon
    loglog(hMin, RError(:, iE), 'linewidth', 2)
    hold on
end

xlabel('h')
ylabel('relative error for fixed epsilon')
title('NyR')

end

%%
function h = PlotGreySpheroid(a, c, clr)

% Create dense array of points
NGrid = 100; 
R = linspace(- 5.1, 5.1, NGrid);
[x,y,z] = meshgrid(R, R, R);

f = x.^2 / a^2 + y.^2 / c^2 + z.^2 / c^2 - 1;

% Calculate isosurface
isoval = 0;
[faces,verts,colors] = isosurface(x, y, z, f, isoval, 0 * f);

h = patch('Vertices',verts,'Faces',faces,'FaceVertexCData',colors,...
    'FaceColor',clr,'EdgeColor','none');
hold on
axis equal vis3d

end
