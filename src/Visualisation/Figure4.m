function Figure4(dataDir)
%FIGURE4 Plot figure 4
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%%
figure

clrs = parula(255);

%% Collate data
load(fullfile(dataDir, 'torus_ne.mat'), 'sol')
zEndNe = sol.y(3, end);

load(fullfile(dataDir, 'torus_ny.mat'), 'hMin', 'nEpsilon', 'sol', 'epsilonN')

hNy = hMin;
nH = numel(hNy);

zEndNy = NaN(nH, nEpsilon);
for iH = 1 : nH
    for iE = 1 : nEpsilon
        try
            zEndNy(iH, iE) = sol{iH, iE}.y(3, end);
        catch
        end
    end
end

load(fullfile(dataDir, 'torus_ny_r.mat'), 'hMin', 'nEpsilon', 'sol')

zEndNyR = NaN(nH, nEpsilon);
for iH = 1 : nH
    for iE = 1 : nEpsilon
        try
            zEndNyR(iH, iE) = sol{iH, iE}.y(3, end);
        catch
        end
    end
end

% Compare results to Ne result
nyEr = abs((zEndNy - zEndNe) ./ zEndNe);
nyREr = abs((zEndNyR - zEndNe) ./ zEndNe);

%% Nystrom
subplot(2, 2, 1)
ConvergenceBoxPlot(fliplr(epsilonN), flipud(hMin), rot90(nyEr, 2), ...
    'fixedWidth', ...
    [1e-4, 1e0], ...
    clrs, ...
    'log')

xlabel('epsilon')
ylabel('h')
title('Ny - Z position at t = 1')

%% Nystrom vary h
subplot(2, 2, 3)
for iE = 1 : nEpsilon
    try
        loglog(hNy, (nyEr(:, iE)))
        hold on
    catch
    end
end

xlabel('h')
ylabel('Z position at t = 1')
title('Ny')

%% Nystrom R
subplot(2, 2, 2)
ConvergenceBoxPlot(fliplr(epsilonN), flipud(hMin), rot90(nyREr, 2), ...
    'fixedWidth', ...
    [1e-4, 1e0], ...
    clrs, ...
    'log')

xlabel('epsilon')
ylabel('h')
title('NyR - Z position at t = 1')

%% Nystrom R vary h
subplot(2, 2, 4)
for iE = 1 : nEpsilon
    try
        loglog(hNy, (nyREr(:, iE)))
        hold on
    catch
    end
end

xlabel('h')
ylabel('Z position at t = 1')
title('Ny')

end
