function Figure3(dataDir)
%FIGURE3 Plot figure 3
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%%
figure

%% Colours
clr_gray_surface = [0.5, 0.5, 0.5];
clrs = parula(255);

subplot(3, 3, 1)
PlotGreyTorus(1, 1 / 0.4, clr_gray_surface);

subplot(3, 3, 3)
PlotGreyTorus(1, 1 / 0.4, clr_gray_surface);

%% Force discr
x = DiscretiseTorus(1 / 0.4, 1, 0.5, [0; 0; 0]);
[x1, x2, x3] = ExtractComponents(x);

subplot(3, 3, 1)
hold on
plot3(x1, x2, x3, '.')

subplot(3, 3, 3)
hold on
plot3(x1, x2, x3, '.')

%% Quadrature discr
x = DiscretiseTorus(1 / 0.4, 1, 0.3, [0; 0; 0]);
[x1, x2, x3] = ExtractComponents(x);

subplot(3, 3, 3)
hold on
plot3(x1, x2, x3, '.')

%% Collate data
load(fullfile(dataDir, 'torus_ne.mat'), 'sol')
zEndNe = sol.y(3, end);

load(fullfile(dataDir, 'torus_ny.mat'), 'hMin', 'nEpsilon', 'sol', 'swimmer')

hNy = hMin;
nH = numel(hNy);

zEndNy = NaN(nH, nEpsilon);
for iH = 1 : nH
    for iE = 1 : nEpsilon
        try
            zEndNy(iH, iE) = sol{iH, iE}.y(3, end);
        catch
        end
    end
end

sDOF = NaN(numel(swimmer), 1);
for iS = 1 : numel(swimmer)
    sDOF(iS) = numel(swimmer{iS}{1}.model.x);
end

load(fullfile(dataDir, 'torus_ny_r.mat'), 'nEpsilon', 'sol')

zEndNyR = NaN(nH, nEpsilon);
for iH = 1 : nH
    for iE = 1 : nEpsilon
        try
            zEndNyR(iH, iE) = sol{iH, iE}.y(3, end);
        catch
        end
    end
end

minZ = min([zEndNy(:) ; zEndNyR(:)]);
maxZ = max([zEndNy(:) ; zEndNyR(:)]);

%% DOF
load(fullfile(dataDir, 'torus_ny.mat'), 'hMin', 'epsilonN')

subplot(3, 3, 2)
loglog(hMin, sDOF(:, 1))
xlabel('h')
ylabel('sDOF')

%% Nystrom
subplot(3, 2, 3)
ConvergenceBoxPlot(fliplr(epsilonN), flipud(hMin), rot90(zEndNy, 2), ...
    'fixedWidth', ...
    [minZ, maxZ], ...
    clrs, ...
    'lin')

xlabel('epsilon')
ylabel('h')
title('Ny - Z position at t = 1')

%% Nystrom vary h
subplot(3, 2, 5)
for iE = 1 : nEpsilon
    try
        semilogx(hNy, (zEndNy(:, iE)))
        hold on
    catch
    end
end

yline((zEndNe), 'k--')
xlabel('h')
ylabel('Z position at t = 1')
title('Ny')

%% Nystrom R
subplot(3, 2, 4)
ConvergenceBoxPlot(fliplr(epsilonN), flipud(hMin), rot90(zEndNyR, 2), ...
    'fixedWidth', ...
    [minZ, maxZ], ...
    clrs, ...
    'lin')

xlabel('epsilon')
ylabel('h')
title('NyR - Z position at t = 1')

%% Nystrom R vary h
subplot(3, 2, 6)
for iE = 1 : nEpsilon
    try
        semilogx(hNy, (zEndNyR(:, iE)))
        hold on
    catch
    end
end

yline((zEndNe), 'k--')
xlabel('h')
ylabel('Z position at t = 1')
title('NyR')

end

%%
function h = PlotGreyTorus(r, R, clr)

% Create dense array of points
NGrid = 100; 
s = linspace(-3 * R, 3 * R, NGrid);
[x,y,z] = meshgrid(s, s, s);

f = (sqrt(x.^2 + y.^2) - R).^2 + z.^2 - r^2;

% Calculate isosurface
isoval = 0;
[faces,verts,colors] = isosurface(x, y, z, f, isoval, 0 * f);

h = patch('Vertices',verts,'Faces',faces,'FaceVertexCData',colors,...
    'FaceColor',clr,'EdgeColor','none');
hold on
axis equal vis3d

end
