function varargout = SolveSedimentingProblem(tRange, ...
    swimmer, boundary, epsilon, domain, blockSize, solveFlag, ...
    procFlag, probFlag, NNMatrices, varargin)
%SOLVESEDIMENTINGPROBLEM Solves the swimming problem for a swimmer sedimenting
%under gravity
%
% SOL = SOLVESEDIMENTINGPROBLEM(TRANGE, SWIMMER, BOUNDARY, EPSILON, DOMAIN,
% BLOCKSIZE, SOLVEFLAG, PROCFLAG, PROBFLAG, NNMATRICES)
%
% SOL = SOLVESEDIMENTINGPROBLEM(TRANGE, SWIMMER, BOUNDARY, EPSILON, DOMAIN,
% BLOCKSIZE, SOLVEFLAG, PROCFLAG, PROBFLAG, NNMATRICES, PASSFORCES)
%
% [T, Z] = SOLVESEDIMENTINGPROBLEM(TRANGE, SWIMMER, BOUNDARY, EPSILON, DOMAIN,
% BLOCKSIZE, SOLVEFLAG, PROCFLAG, PROBFLAG, NNMATRICES)
%
% [T, Z] = SOLVESEDIMENTINGPROBLEM(TRANGE, SWIMMER, BOUNDARY, EPSILON, DOMAIN,
% BLOCKSIZE, SOLVEFLAG, PROCFLAG, PROBFLAG, NNMATRICES, PASSFORCES)
%
% input:  
%        TRANGE      Range of times for ODE solver
%        SWIMMER     struct describing how to construct swimmer
%        BOUNDARY    struct describing how to construct boundary
%        EPSILON     regularisation parameter
%        DOMAIN      'i' for infinite fluid, 'h' for half space above no-slip boundary (x3>0)
%        BLOCKSIZE   controls matrix assembly blocking size for numerical
%                     efficiency, 0.2 is a safe choice (GB)
%        SOLVEFLAG   Choice of ODE solver - ode45, ode113
%        PROCFLAG    Processing flag - 'cpu' or 'gpu'
%        PROBFLAG    'richardson' or '' for nystrom
%        NNMatrices  {NN, NNSW} - nearest-neighbour matrices for all points
%                       and swimmer only

%        PASSFORCES  bool - Pass out the vector of calculated forces

% output: 
%        SOL         MATLAB ode solver solution struct
%
%        T           Vector of solved timepoints
%        Z           position/orientation of swimmers at T
%                     z{s}(1:3) = x0   - origin of swimmer
%                     z{s}(4:6) = b1   - first basis vector of swimmer frame
%                     z{s}(7:9) = b2   - second basis vector of swimmer frame
%       
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

nSw = length(swimmer);

% Calculate DOF
DOF = 0;
for iSw = 1 : nSw
    if strcmp(probFlag, 'richardson')
        swimmer{iSw}.model.X = swimmer{iSw}.model.X1;
    end
    [xForce,~,~] = swimmer{iSw}.fn(0,swimmer{iSw}.model);
    DOF = DOF + length(xForce);
end

if ~isempty(boundary)
    [xQuad,~] = boundary.fn(boundary.model);
    DOF = DOF + length(xQuad);
end

fprintf(['Solving time-dependent swimming ', ...
    'problem with %i DOF...\n'],DOF)

z0 = zeros(9*nSw + DOF, 1);
for iSw = 1 : nSw
    z0(iSw)         = swimmer{iSw}.x0(1);
    z0(nSw   + iSw) = swimmer{iSw}.x0(2);
    z0(2*nSw + iSw) = swimmer{iSw}.x0(3);
    z0(3*nSw + iSw) = swimmer{iSw}.b10(1);
    z0(4*nSw + iSw) = swimmer{iSw}.b10(2);
    z0(5*nSw + iSw) = swimmer{iSw}.b10(3);
    z0(6*nSw + iSw) = swimmer{iSw}.b20(1);
    z0(7*nSw + iSw) = swimmer{iSw}.b20(2);
    z0(8*nSw + iSw) = swimmer{iSw}.b20(3);
end

if isempty(NNMatrices)
    NN = [];
    NNSw = [];
else
    NN = NNMatrices{1};
    NNSw = NNMatrices{2};
end

if isempty(varargin)
    keepForces = [];
else
    keepForces = 1;
end


switch solveFlag
    case 'ode45'
        
        clearvars testprogressbar
        testprogressbar = []; %#ok<NASGU>
        odeopts = odeset('OutputFcn',@odetpbar);

        if nargout == 1
            switch probFlag
                case 'richardson'
                    varargout{1} = ode45(@(t,z) SedimentingProblemRichardson(z,swimmer, ...
                        boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                        procFlag,keepForces), ...
                        tRange,z0,odeopts);
                otherwise
                    varargout{1} = ode45(@(t,z) SedimentingProblem(z,swimmer, ...
                        boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                        procFlag,keepForces), ...
                        tRange,z0,odeopts);
            end
            
        else
            switch probFlag
                case 'richardson'
                    [varargout{1},varargout{2}]=ode45(@(t,z) SedimentingProblemRichardson( ...
                        z,swimmer,boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                        procFlag,keepForces), ...
                        tRange,z0,odeopts);
                otherwise
                    [varargout{1},varargout{2}]=ode45(@(t,z) SedimentingProblem( ...
                        z,swimmer,boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                        procFlag,keepForces), ...
                        tRange,z0,odeopts);
            end
        end
        
    case 'ode113'
        if nargout == 1
            varargout{1} = ode113(@(t,z) SedimentingProblem(z,swimmer, ...
                boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                procFlag,keepForces), ...
                tRange,z0);
            
        else
            [varargout{1},varargout{2}]=ode113(@(t,z) SedimentingProblem( ...
                z,swimmer,boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                procFlag,keepForces), ...
                tRange,z0);
        end
        
    otherwise
        error('Solve flag not known')
end

end