function BT = CalcRotationForceCouplingMatrixRichardson(x, X, X0, epsilon, domain, blockSize, varargin)
%CALCROTATIONFORCECOUPLINGMATRIXRICHARDSON calculates the rotation force
%coupling matrix for an object from its body frame points
%
% C = CALCROTATIONFORCECOUPLINGMATRIXRICHARDSON(x, X, X0, epsilon, domain, blockSize)
%
% C = CALCROTATIONFORCECOUPLINGMATRIXRICHARDSON(x, X, X0, epsilon, domain, blockSize, procFlag)
%
% C = CALCROTATIONFORCECOUPLINGMATRIXRICHARDSON(x, X, X0, epsilon, domain, blockSize, procFlag, NN)
%
% input:  
%         x          body frame collocation/force points
%         X          body frame stokeslet points
%         X0         body frame origin
%         eps        umerical regularisation lengths for Richardson
%         domain     'i' for infinite fluid, 'h' for half space above no-slip boundary (x3>0)
%         blockSize  controls matrix assembly blocking size for numerical efficiency, 0.2 is a safe choice
%         procFlag   Processing flag - 'cpu' or 'gpu'
%         NN         NEAREST neighbour matrix 
% output: A          3x3 matrix of rotation/force coefficients 
%                    (scaled wrt 1 / mu)
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

if isempty(varargin)
    procFlag = 'cpu';
    fprintf(' 1')
    [F1, ~, ~, ~, NN] = SolveRigidResistanceRichardson(x, X, X0, [0, 0, 0], [1, 0, 0], epsilon, domain, blockSize, procFlag);
elseif length(varargin) == 1
    procFlag = varargin{1};
    fprintf(' 1')
    [F1, ~, ~, ~, NN] = SolveRigidResistanceRichardson(x, X, X0, [0, 0, 0], [1, 0, 0], epsilon, domain, blockSize, procFlag);
else
    procFlag = varargin{1};
    NN = varargin{2};
    fprintf(' 1')
    F1 = SolveRigidResistanceRichardson(x, X, X0, [0, 0, 0], [1, 0, 0], epsilon, domain, blockSize, procFlag, NN);
end

fprintf(' 2')
F2 = SolveRigidResistanceRichardson(x, X, X0, [0, 0, 0], [0, 1, 0], epsilon, domain, blockSize, procFlag, NN);
fprintf(' 3\n')
F3 = SolveRigidResistanceRichardson(x, X, X0, [0, 0, 0], [0, 0, 1], epsilon, domain, blockSize, procFlag, NN);

BT = [F1, F2, F3];

end