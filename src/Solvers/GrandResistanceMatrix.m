function [R, walltime, RError, h, sDOF] = ...
    GrandResistanceMatrix(x, X, NN, X0, R_an, epsilon, domain, blockSize, procFlag)
%GRANDRESISTANCEMATRIX Calculates the GRM
%
% [R, WALLTIME, ERROR, H, SDOF] = GRANDRESISTANCEMATRIX(X, XX, NN,
% X0, RAN, EPSILON, DOMAIN, BLOCKSIZE, PROCFLAG) calculates the GRM for a
% prolate spheroid using the method of regularised stokeslets.
% 
% Inputs:
%   X - force discretisation
%   XX - quadrature discretisation (X = XX - Nystrom, otherwise NEAREST)
%   NN - Nearest Neighbour matrix (or speye(size(X)) for Nystrom)
%   X0 - Location of prolate spheroid
%   RAN - Anayltical solution for GRM
%   EPSILON - Regularisation parameter
%   DOMAIN - 'i' for stokeslets, 'h' for blakelets
%   BLOCKSIZE - Parameter to limit memory use (GB)
%   PROCFLAG - Processor flag 'cpu' or 'gpu'
%
% Outputs:
%   R - GRM
%   WALLTIME - elapsed CPU time
%   ERROR - Relative error in calculating the GRM compared to RAN
%   H - Minimum spacing between force points
%   SDOF - Number of scalar degrees of freedom
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

% Walltime start
tic

% Calculate grand resistance tensor
A = CalcTranslationalResistanceMatrix(x, X, X0, epsilon, domain, blockSize, procFlag, NN);
B = CalcTranslationMomentCouplingMatrix(x, X, X0, epsilon, domain, blockSize, procFlag, NN);
BT = CalcRotationForceCouplingMatrix(x, X, X0, epsilon, domain, blockSize, procFlag, NN);
C = CalcRotationalResistanceMatrix(x, X, X0, epsilon, domain, blockSize, procFlag, NN);
R = [A, BT; B, C];

% Walltime end
walltime = toc;

% Calculate errors
RError = norm(R - R_an) / norm(R_an);

% Min h for the discretisation
h = CalcDiscr_h(x, blockSize);

% Scalar egrees of freedom
sDOF = length(x);

end