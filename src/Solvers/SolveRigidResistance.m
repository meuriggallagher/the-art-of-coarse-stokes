function [FF, MM, f, condno, NN] = SolveRigidResistance(x, X, X0, U, Om, ep, domain, blockSize, varargin)
%SOLVERIGIDRESISTANCE Solves the rigid body resistance problem with nearest neighbour regularised stokeslets, force nodes = coll nodes
%
% [FF, MM, F, CONDNO, NN] = SOLVERIGIDRESISTANCE(x, X, X0, U, OM, epsilon, domain, blockSize)
%
% [FF, MM, F, CONDNO, NN] = SOLVERIGIDRESISTANCE(x, X, X0, U, OM, epsilon, domain, blockSize, PROCFLAG)
%
% [FF, MM, F, CONDNO, NN] = SOLVERIGIDRESISTANCE(x, X, X0, U, OM, epsilon, domain, blockSize, PROCFLAG, BLOCKSIZE)
%
% Input:  x          collocation/traction points with all x1 coords first, then all x2...
%         X          stokeslet points
%         X0         origin of rotation
%         U, Om      vectors with rigid body velocity and angular velocity
%         ep         regularisation parameter
%         domain     'i' infinite fluid (regularised stokeslet) or 'h' halfspace x3 > 0 (regularised blakelet)
%         blockSize  maximum memory storage for stokeslet matrix in GB
%         procFlag   Processing flag - 'cpu' or 'gpu'
%
% Output: FF         vector of force on body
%         MM         vector of moment on body
%         F          stokeslet strengths
%         CONDNO     condition number of the matrix
%         NN         nearest-neighbour matrix
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

if isempty(varargin)
    procFlag = 'cpu';
    
    NN = [];
else
    procFlag = varargin{1};
    
    if length(varargin) > 1
        NN = varargin{2};
    else
        NN = [];
    end
end

M = length(x) / 3;

%% lhs assembly
if isempty(NN)
    [AS, NN] = AssembleStokesletMatrix(x, X, x, ep, domain, blockSize, procFlag);
else
    [AS, NN] = AssembleStokesletMatrix(x, X, x, ep, domain, blockSize, procFlag, NN);
end

%% rhs assembly

[x1, x2, x3]=ExtractComponents(x);

u1 = U(1)*ones(M,1);
u2 = U(2)*ones(M,1);
u3 = U(3)*ones(M,1);

x1 = x1 - X0(1);
x2 = x2 - X0(2);
x3 = x3 - X0(3);

% RHS
rhs = [u1; u2; u3] + [Om(2) * x3 - Om(3) * x2 ; Om(3) * x1 - Om(1) * x3 ; Om(1) * x2 - Om(2) * x1];

% if strcmp(procFlag, 'cpu')
%     condno = cond(AS);
% elseif strcmp(procFlag, 'gpu')
%     condno = NaN;
% end
condno = NaN;

%% Solve
f = AS \ rhs;

if strcmp(procFlag, 'gpu')
    f = gather(f);
end

%% Extract 
% calculate total force from force distribution
[F1, F2, F3] = ExtractComponents(NN * f);

FF = [sum(F1); sum(F2); sum(F3)];

% calculate moment
[X1, X2, X3] = ExtractComponents(X);

X1 = X1 - X0(1);
X2 = X2 - X0(2);
X3 = X3 - X0(3);

x = [X1', X2', X3'] * NN;
[x1, x2, x3] = ExtractComponents(x);
Ze = 0 * x1;

AM =[Ze, -x3, x2 ; x3, Ze, - x1 ; - x2, x1, Ze];

MM = AM * f;

end
