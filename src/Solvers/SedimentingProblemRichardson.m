function dz = SedimentingProblemRichardson(z,swimmer,boundary,t,epsilon, ...
    domain, blockSize, NN, NNsw,procFlag,varargin)
%SEDIMENTINGPROBLEMRICHARDSON constructs and solves the matrix system for a
%sedeminting object using NEARSET
%
% DZ = SEDIMENTINGPROBLEMRICHARDSON(Z, SIWMMER, BOUNDARY, T, EPSILON, DOMAIN, BLOCKSIZE, NN, NNSW, PROCFLAG)
%
% DZ = SEDIMENTINGPROBLEMRICHARDSON(Z, SIWMMER, BOUNDARY, T, EPSILON, DOMAIN, BLOCKSIZE, NN, NNSW, PROCFLAG, PASSFORCES)
%
% input:  
%        Z{s}        position/orientation of swimmer {s}
%                     z{s}(1:3) = x0   - origin of swimmer
%                     z{s}(4:6) = b1   - first basis vector of swimmer frame
%                     z{s}(7:9) = b2   - second basis vector of swimmer frame
%        SWIMMER     struct describing how to construct swimmer
%        BOUNDARY    struct describing how to construct boundary
%        T           time (scalar)
%        EPSILON     regularisation parameters for Richardson extrapolation
%        DOMAIN      'i' for infinite fluid, 'h' for half space above no-slip boundary (x3>0)
%        BLOCKSIZE   controls matrix assembly blocking size for numerical
%                     efficiency, 0.2 is a safe choice (GB)
%        NN          NEAREST neighbour matrix for all points
%        NNSW        NEAREST neighbour matrix for swimmer points only
%        procFlag    Processing flag - 'cpu' or 'gpu'

% output: 
%        DZ          Rate of change of vector of unkowns
%       
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%% Establish number of swimmers
nSw = length(swimmer);
% t

%% Obtain swimmer points
sol = cell(3,1);
for iRich = 1 : 3
    if iRich == 1
        swimmer{1}.model.X = swimmer{1}.model.X1;
    elseif iRich == 2
        swimmer{1}.model.X = swimmer{1}.model.X2;
    else
        swimmer{1}.model.X = swimmer{1}.model.X3;
    end
    
    [xForce,vForce,xQuad,xForceSwRotate,indSw,b1,b2,xQuadSwRotate] ...
        = GetSwimmerPoints(swimmer,z,t,'cpu');
    
    
    %% Extract boundary points
    if ~isempty(boundary)
        [xForceBnd,xQuadBnd] = boundary.fn(boundary.model);
        vBnd = 0 * xForceBnd;
    else
        xForceBnd = [];
        xQuadBnd = [];
        vBnd = [];
    end
    
    % Merge swimmer and boundary points
    x = MergeVectorGrids(xForce, xForceBnd);
    X = MergeVectorGrids(xQuad, xQuadBnd);
    v = MergeVectorGrids(vForce, vBnd);
    
    
    %% Assemble matrix system - mobility problem
    NSw = length(xForce)/3;
    QSw = length(xQuad)/3;
    NBnd = length(xForceBnd)/3;
    N = NSw + NBnd;
    
    %% Assemble stokeslet matrix
    switch procFlag
        case 'gpu'
            xF = gpuArray(x);
            xQ = gpuArray(X);
            
            [AS,~]=AssembleStokesletMatrix(xF,xQ,xF,epsilon(iRich), ...
                domain,blockSize,procFlag,NN{iRich});
            
        case 'cpu'
            [AS,~]=AssembleStokesletMatrix(x,X,x,epsilon(iRich), ...
                domain,blockSize,procFlag,NN{iRich});
    end
    
    %% AU block
    % component of velocity due to translational velocity of swimmers;
    %   zero velocity of boundary
    au = zeros(NSw+NBnd,nSw);
    for n = 1 : nSw
        au(indSw(n):indSw(n+1)-1,n) = -ones(indSw(n+1)-indSw(n),1);
    end
    AU = kron(eye(3),au);
    
    %% AF - force summation - only on swimmer
    af = zeros(nSw, N);
    for n= 1 : nSw
        af(n,indSw(n):indSw(n+1)-1) = sum(NNsw{iRich}(1:QSw,indSw(n):indSw(n+1)-1),1);
    end
    AF = kron(eye(3),af);
    
    %% A0m - component of velocity due to rotation of swimmer about x0;
    %   zero velocity of boundary
    Ze  = zeros(N, nSw);
    x1m = zeros(N, nSw);
    x2m = zeros(N, nSw);
    x3m = zeros(N, nSw);
    for n = 1 : nSw
        [x1,x2,x3]=ExtractComponents(xForceSwRotate{n});
        x1m(indSw(n):indSw(n+1)-1,n) = x1;
        x2m(indSw(n):indSw(n+1)-1,n) = x2;
        x3m(indSw(n):indSw(n+1)-1,n) = x3;
    end
    AOm = [ Ze -x3m x2m; x3m Ze -x1m; -x2m x1m Ze];
    
    %% AM
    Ze  = zeros(nSw,N);
    x1m = zeros(nSw,N);
    x2m = zeros(nSw,N);
    x3m = zeros(nSw,N);
    for n=1:nSw
        [x1,x2,x3]=ExtractComponents(xQuadSwRotate{n}'*NNsw{iRich}); % moment summation
        x1m(n,indSw(n):indSw(n+1)-1) = x1;
        x2m(n,indSw(n):indSw(n+1)-1) = x2;
        x3m(n,indSw(n):indSw(n+1)-1) = x3;
    end
    AM = [Ze -x3m x2m; x3m Ze -x1m; -x2m x1m Ze];
    
    %% A
    A = [ AS, AU, AOm            ; ...
        AF, zeros(3*nSw,6*nSw) ; ...
        AM, zeros(3*nSw,6*nSw)];
    
    %% Assemble RHS of problem
    b = [v ; zeros(6*nSw,1)];
    
    % Gravitational force
    for n = 1 : nSw
        b(size(AS, 1) + 2 * nSw + n) = - 1;
    end
    
    %% Solve problem
    sol{iRich} = A \ b;
    
    switch procFlag
        case 'gpu'
            sol{iRich}  = gather(sol{iRich});
    end
end

% Richardson step
IM3 = RichardsonMatrixInverse(epsilon);
sol = IM3(1, 1) * sol{1} + IM3(1, 2) * sol{2} + IM3(1, 3) * sol{3};

%% Extract solution
% Initialise output
if isempty(varargin)
    dz = NaN(9*nSw, 1);
else
    dz = NaN(9*nSw+3*N, 1);
end

% Velocities
U = sol(3*N+1       : 3*N+3*nSw);
dz(1 : 3*nSw, 1)  = U;

% Angular velocities
Om= sol(3*N+3*nSw+1 : 3*N+6*nSw);
for n = 1 : nSw
    om = Om(n : nSw : n + 2*nSw);
    dz(3*nSw+n:nSw:n+5*nSw,1)  = cross(om, b1{n});
    dz(6*nSw+n:nSw:n+8*nSw,1)  = cross(om, b2{n});
end

% Forces
if ~isempty(varargin)
    f = sol(1 : 3*N);
    dz(9*nSw+1:9*nSw+3*N,1) = f;
end

end
