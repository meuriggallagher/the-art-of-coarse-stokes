%MAIN runs test problems and plots
%
% Code for the manuscript:
%   "The Art of Coarse Stokes: Richardson extrapolation improves the accuracy and efficiency of the method of regularized stokeslets" 
% D.J. Smith & M.T. Gallagher
%

% Author: M.T. Gallagher
% Email: m dot t dot gallagher at bham dot ac dot uk
% www.gitlab.com/meuriggallagher
% Copyright: M.T. Gallagher 2020

%% Add paths
p1 = genpath('src');
addpath(p1)

p2 = genpath('bin');
addpath(p2)

%% Directory for data output
% Current dir
currentFile = mfilename( 'fullpath' );
[pathstr,~,~] = fileparts( currentFile );
dataDir = fullfile(pathstr, 'data');

if ~exist(dataDir, 'dir')
    mkdir(dataDir)
end

%% Plot all tests
procFlag = 'cpu';
blockSize = 1;

RunAllTestProblems(procFlag, blockSize, dataDir);

%% Plot data
PlotFiguresFromManuscript(dataDir);

%% Remove paths
rmpath(p1)
rmpath(p2)
